import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * This script is called by the ServiceBroker zipPhaseDocs method in order to query
 * the UCM database for all document ids and labels for a given CARAID and CARAPHASEID pair.
 * It then formats the results into the request format expected by the DMD Downloader and
 * issues a post request to the given URL for further processing.
 *
 *
 * @author sam
 *
 */
public class CARADownloadJobBuilder {

	private final String USER_AGENT = "Mozilla/5.0";

	//private  String stage_db = "jdbc:oracle:thin:@54.146.18.74:1521:coreprod2";
	private String connectionStr;
	private String username;
	private String password;

	private String dm_connectionStr;
	private String dm_username;
	private String dm_password;


	private Connection connection = null;
	private Statement statement = null;
	private ResultSet resultSet = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		try{
			String connString = args[0];
			String username = args[1];
			String password = args[2];
			String dm_connString = args[3];
			String dm_username = args[4];
			String dm_password = args[5];
			String caraid = args[6];
			String caraphaseid = args[7];
			String email = args[8];
			String usershortname = args[9];
			String downloaderUrl = args[10];
			String columns = args[11];
			String archiveRoot = args[12];
			String formsetid = "";
			if (args.length > 13) {
				formsetid = args[13];
			}

			CARADownloadJobBuilder utility = new CARADownloadJobBuilder(connString, username, password, dm_connString, dm_username, dm_password);
			String docIDs = "";
			BufferedReader flr = null;
			Map docMap;
			if (formsetid != "") {
				try {
					URL jarLocation = CARADownloadJobBuilder.class
							.getProtectionDomain()
							.getCodeSource()
							.getLocation();

					String formlistpath =  new File(jarLocation.toString()).getParent() + "/formlists/"
							+ caraid + "-"
							+ caraphaseid + "-"
							+ usershortname + "-"
							+ formsetid + ".txt";

					if (formlistpath.startsWith("file:")) {
						formlistpath = formlistpath.substring(5);
					}
					flr = new BufferedReader(new FileReader(formlistpath));
					docIDs = flr.readLine();
				} catch(Exception e) {
					e.printStackTrace();
					//download will fail anywy, alert PHP calling function that the download failed
					throw e;
				} finally {
					if (null != flr) { flr.close(); }
				}
				docMap = utility.getDocTitles(docIDs);
			} else {
				docMap = utility.getCaraPhaseDocs(caraid, caraphaseid);
			}
			String postPayload = utility.constructDownloaderPayload(caraid, caraphaseid, formsetid,
					email, usershortname, columns, archiveRoot, docMap);

			utility.sendPost(downloaderUrl, postPayload);

		}catch(Exception e){
			e.printStackTrace();
			//download will fail anyway, alert PHP calling function that the download failed
			throw e;
		}

	}

	public CARADownloadJobBuilder(){}

	public CARADownloadJobBuilder(String connectionStr, String username, String password, String dm_connectionStr, String dm_username, String dm_password){
		this.connectionStr = connectionStr;
		this.username = username;
		this.password = password;
		this.dm_connectionStr = dm_connectionStr;
		this.dm_username = dm_username;
		this.dm_password = dm_password;

	}

	private void initializeDB() throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection(dm_connectionStr, dm_username, dm_password);
	}

	private void initializeUCMDB() throws Exception{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		connection = DriverManager.getConnection(connectionStr, username, password);
	}


	private void close() throws Exception{
		statement.close();
		resultSet.close();
		connection.close();
	}

	//check and see, is this Project in Datamart or Not
	public boolean isDatamart(String cara_id, String phase_id) throws Exception{
		boolean result = false;

		initializeDB();
		statement = connection.createStatement();
		String sql ="SELECT * FROM datamart1.CARAProjects where Id=" + cara_id;
		resultSet= statement.executeQuery(sql);

		if(resultSet.next()){
			result=true;
		}
		close();
		return result;
	}

	//select d.XMOREMETADATA from DOCMETA d, REVISIONS r where r.DID = d.DID and r.DDOCNAME = 'FSPLT3_1629331';
	//select query changed to datamart mysql db on Sep 29th, 2016 by OJAS & SARAVANA
	public Map getCaraPhaseDocs(String cara_id, String phase_id) throws Exception{
		Map result = new HashMap();

		String sql = "select t3.documentid DDOCNAME, t3.label DDOCTITLE"
				+ " from datamart1.CARAProjects t1,"
				+ " datamart1.CommentPhases t2,"
				+ " datamart1.ProjectDocuments t3,"
				+ " datamart1.ProjectDocumentContainers t4 "
				+ " where  t2.phaseid =" + phase_id + "  and t2.caraid =" + cara_id + " and ( t4.palscontid = 57 or  t4.palscontid = 92 ) "
				+ " and t3.containerid = t4.id "
				+ " and t2.caraid = t1.id "
				+ " and t1.projectid = t3.projectid "
				+ " and t4.projectid = t3.projectid "
				+ " and t2.phaseid = t3.phaseid ";


		if(isDatamart(cara_id,phase_id)){
			initializeDB();
			statement = connection.createStatement();
		}else{
			initializeUCMDB();
			statement = connection.createStatement();
			sql = "SELECT R.DDOCNAME, R.DDOCTITLE from REVISIONS R, DOCMETA D WHERE R.DID=D.DID"
					+ " and D.XMOREMETADATA LIKE '%\"caraid\":\""+cara_id+"\"%'"
					+ " and D.XMOREMETADATA LIKE '%\"caraphaseid\":\""+phase_id+"\"%'";
		}

		resultSet= statement.executeQuery(sql);

		while(resultSet.next()){
			result.put(resultSet.getString("DDOCNAME"),
					resultSet.getString("DDOCTITLE"));
		}
		close();
		return result;
	}

	public Map getDocTitles(String docIDs) throws Exception{
		Map result = new HashMap();

		String[] documentIDs = docIDs.split(",");
		for (int i=0; i < documentIDs.length; i++){
			documentIDs[i] = "\"" + documentIDs[i] + "\"";
		}
		String IDs = String.join(",", documentIDs);

		String sql = "SELECT DocumentId DDOCNAME, Label DDOCTITLE FROM ProjectDocuments WHERE DocumentId IN ("
				+ IDs + ");";

		initializeDB();
		statement = connection.createStatement();

		resultSet= statement.executeQuery(sql);

		while(resultSet.next()){
			result.put(resultSet.getString("DDOCNAME"),
					resultSet.getString("DDOCTITLE"));
		}
		close();
		return result;
	}

	public String constructDownloaderPayload(String caraid, String phaseid,
											 String formid, String userEmail,
											 String username, String columns,
											 String archiveRoot, Map docMap){

		String archiveName = "CARA_"+caraid+"_PHASE_"+phaseid+"_"+formid;
		String payload = "userEmail="+userEmail+"&archiveRoot="+archiveRoot
				+"&userID="+username+"&downloadSpec="+archiveName+"|"+columns
				+"|"+caraid;


		//Keys are the Doc IDs for all results
		java.util.Iterator keyIter = docMap.keySet().iterator();
		int count = 0;
		//Append all document IDs and Titles to request string
		while(keyIter.hasNext()){

			String doc_id = (String) keyIter.next();
			//$val = $result_array[$doc_id];
			String val = (String) docMap.get(doc_id);
			//NOTE: value is 'reencoded' by the time POST reaches buildjobNonPals.
			//Replacing commas directly with underscores for CSV.
			String doc_title = val.replace(",", "_");
			doc_title = doc_title.replaceAll("[^A-Za-z0-9()_\\[\\]]", "_");
			payload = payload+"|"+archiveRoot+","+doc_title+","+doc_id;
			count++;

		}
		payload = payload+"&totalFiles="+count;

		return payload;
	}

	// HTTP POST request
	public void sendPost(String url, String urlParameters) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	}
}
