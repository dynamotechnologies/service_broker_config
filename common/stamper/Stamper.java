import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.awt.geom.PathIterator;
import java.awt.geom.AffineTransform;
import java.awt.Color;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.io.RandomAccessBuffer;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/*
 *  Bates-stamp list of documents from job file
 *
 *  Exit codes: 0 - success
 *              1 - general error
 *              2 - invalid PDF input
 *
 *  The input file is the "job file" which consists of a config section
 *  (each line consisting of a config name, a colon, and a config value)
 *  followed by an end-of-configs line (::) and a list of paths to PDF files
 *  to be stamped.
 *
 *  List of job file configs:
 *
 *    location:topleft
 *             topright
 *             bottomleft
 *             bottomright
 *
 *    addmargin:true|false (default false)
 *
 *    prefix:string (default none)
 *
 *    suffix:string (default none)
 *
 *    totalfiles:number of files (default count files in job conf)
 *    startfileseq:first file number (only for fileseq)
 *    startpageincollection:first page number (only for pageincollection)
 *
 *    stampformat:Combination of [date], [prefix], [suffix]:
 *                and the following counters:
 *                    [totalfiles] (number of files in collection)
 *                    [totalfilepages] (number of pages in current file)
 *                    [fileseq] (sequence number of file, default start at 1)
 *                    [pageinfile] (page in file, default start at 1 each file)
 *                    [pageincollection] (page over all files)
 *                Examples:
 *                    [date] [prefix][pageincollection][suffix] (default)
 * [date] [prefix][fileseq][suffix] Page [pageinfile] of [totalfilepages]
 *
 *    There are printf-style specifiers for all counters:
 *    totalfilesformat
 *    totalfilepagesformat
 *    fileseqformat
 *    pageinfileformat
 *    pageincollectionformat
 *
 *    dateformat:Specifiers provided by Java SimpleDateFormat, e.g.
 *                MM/dd/yyyy	(01/20/2012)
 *                MMMM d, yyyy	(January 5, 2012)
 *                MMMM yyyy	(January 2012)
 *                yyyy		(2012)
 *
 *    statictime:long (Unix timestamp--if present, use for all datestamps,
 *                otherwise regenerate each timestamp)
 *
 *    :: (end of options)
 *    followed by list of paths to PDFs to be stamped
 *
 *  The output consists of:
 *      each stamped PDF file in the OUTPUTDIR directory, named [index].pdf
 *          (e.g. 1.pdf, 2.pdf, etc.)
 *      a configuration file (OUTPUTDIR/nextjob.conf) which is updated
 *          after every file is stamped, and is used for job restarts
 *      a log of first-page stamp values for document (OUTPUTDIR/stamplog)
 *          which is updated after every file is stamped, and is used for
 *          the final batch report
 *
 *  A PDF can have 5 different boundary boxes:
 *      Media box - size of maximum render area before clipping, required
 *      Crop box - size of page after cropping, optional
 *      Bleed box - maximum size of physical output, optional
 *      Trim box - size of physical output after trimming, optional
 *      Art box - boundary of meaningful page contents, optional
 *
 *  The bleed and trim boxes default to the crop box, which defaults to the
 *  media box.  But technically if any of these boxes are present, the final
 *  page should be considered to be no larger than the intersection of all
 *  boxes (except for the art box, which is not really a clip boundary).
 *
 *  In practice, most documents seem to omit the crop box, but it does show
 *  up sometimes, and Acrobat Reader will generally clip the document to the
 *  crop box.  (Although I have found exceptions--see getBoundingBox())
 *  So far I haven't seen any documents with bleed or trim boxes, but that's
 *  no guarantee that one won't bite us on the ass in the future.
 */

class Stamper {

static protected float ONE_INCH = 72;
static protected float MARGIN = ONE_INCH / 2;
static protected float MARGINLEADING = MARGIN / 8;

// Define limits for widest and narrowest page allowed before we start scaling
static protected float LIMIT_NARROW = (float)(8.5 * ONE_INCH * 0.8);
static protected float LIMIT_WIDE = (float)(11 * ONE_INCH * 1.2);

static protected String OUTPUTDIR = "output";
static protected String NEXTCONF = "nextjob.conf";
static protected String STAMPLOG = "stamplog";

protected File StampRecordFp = null;

protected List<String> filelist;

protected int TotalFiles = 0;
protected String TotalFilesFormat = "%d";

protected int TotalFilePages = 0;		// Pages in current file	
protected String TotalFilePagesFormat = "%d";

protected String DateFormat = "mm/dd/yyyy";
protected String DateStamp = "";
protected Date StaticTime = null;		// If set, use for date stamp

protected int PageInCollection = 1;		// Current page of file set
protected String PageInCollectionFormat = "%06d";

protected int PageInFile = 0;			// Current page of current file
protected String PageInFileFormat = "%d";

protected int FileSeq = 1;			// Count files
protected String FileSeqFormat = "%d";

protected String Prefix = "";	// Note: include any separator char
protected String Suffix = "";	// Note: include any separator char

protected String StampFormatOrig = "";	// Copy of original format config
protected String[] StampFormat = {};	// Stamp fields
// These are the components of the actual Bates stamp.  All fields are
// concatenated to form the stamp text, with keywords interpreted as necessary.
// Stamp fields are either literal strings, or one of the following:
//   [date]
//   [prefix]
//   [suffix]
//   [totalfiles]
//   [totalfilepages]
//   [fileseq]
//   [pageinfile]
//   [pageincollection]
// Numeric values are formatted according to the corresponding "-format"
// specifier (e.g. totalfiles -> totalfilesformat, etc.)
// The stamp format does not currently permit escape chars (e.g. \[ \])

// Bates stamp font properties
protected PDFont StampFont = PDType1Font.COURIER_BOLD;
protected int StampFontSize = 12;
protected String StampFontFamily = "COURIER";	// n/a for PDFBox

// Bates stamp position on page
protected boolean PositionLeft = true;	// Left or Right
protected boolean PositionTop = true;	// Top or Bottom

// Flag to expand page media/crop box to accomodate Bates stamp
protected boolean AddMargin = false;

/*
 *  Make sure output/ exists and is writable
 *  Make sure output/nextjob.conf does not exist
 */
void sanityCheck() throws IOException, SecurityException {
    // Does output/ exist?
    File outputdir = new File(this.OUTPUTDIR);
    if (!outputdir.exists()) {
        if (!outputdir.mkdir()) {
            die("Cannot mkdir " + this.OUTPUTDIR + ", can't continue");
        }
    }

    // Is output/ writable?
    if (!outputdir.canWrite()) {
        die("Cannot write to directory " + this.OUTPUTDIR + ", can't continue");
    }

    // Don't delete nextjob.conf, it will be reused if this stamp job fails

    String stamplogfilename = this.OUTPUTDIR + "/" + this.STAMPLOG;
    File stamplog = new File(stamplogfilename);
    if (stamplog.exists()) {
        if (!stamplog.delete()) {
            die("Cannot remove " + stamplogfilename + ", can't continue");
        }
    }
}

/*
 *  Get page boundaries
 *
 *  This is usually the crop box or the media box, whichever is smaller
 *
 *  I found test PDFs for which Adobe Reader appeared to clip to the crop box,
 *  and documents for which it refused to clip the display to either the
 *  modified media box or the crop box.  My workaround here is to merge the
 *  vertical boundaries of the crop box and media box, but this is admittedly
 *  a fudge factor.
 *
 *  This function might have to be expanded to use the bleed and trim boxes
 *
 *  The first page is 1
 *
 *  Note that this function clones the PDRectangle so that modifying the
 *  rectangle does not alter the corresponding COS object.
 */
public PDRectangle getBoundingBox(PDPage page) {
    PDRectangle box = cloneRect(page.findMediaBox());
    PDRectangle cropbox = page.findCropBox();
    if (cropbox != null) {
        if (cropbox.getUpperRightY() < box.getUpperRightY()) {
            box.setUpperRightY(cropbox.getUpperRightY());
        }
        if (cropbox.getLowerLeftY() > box.getLowerLeftY()) {
            box.setLowerLeftY(cropbox.getLowerLeftY());
        }
    }
    return box;
}

/*
 *  Duplicate a PDRectangle so that it is independent of any underlying
 *  COS objects attached to the original PDRectangle
 */
public PDRectangle cloneRect(PDRectangle rect) {
    PDRectangle newrect = new PDRectangle();
    newrect.setLowerLeftX(rect.getLowerLeftX());
    newrect.setLowerLeftY(rect.getLowerLeftY());
    newrect.setUpperRightX(rect.getUpperRightX());
    newrect.setUpperRightY(rect.getUpperRightY());
    return newrect;
}

/*
 *  Return page bounding box oriented so +Y is up and +X is to the right
 *  and the origin is in the lower-left corner
 *  (i.e. rotated according to the page /Rotate property)
 *
 *  This gives you the dimensions and orientation of the final page, but
 *  the actual coordinates cannot be applied to the page until its
 *  coordinate space has been transformed to match
 *
 *  The PDF spec says the PDF /Rotate command is just an optional hint for
 *  the page to be rotated for viewing or printing, and can only take values
 *  in multiples of 90.  (Negative values work OK too.)  It makes no changes
 *  to the page coordinate space.
 *
 *  However, anyone viewing the page is going to expect the page orientation
 *  to be with the Y-axis toward the top of the PDF viewer, at least as far as
 *  stamp placement is concerned.  Therefore we need to compensate for page
 *  rotation when creating the Bates stamp.
 */
public PDRectangle getRotatedBoundingBox(PDPage page) {
    PDRectangle rect = getBoundingBox(page);
    float llx = rect.getLowerLeftX();
    float lly = rect.getLowerLeftY();
    float urx = rect.getUpperRightX();
    float ury = rect.getUpperRightY();

    int rotation = findNormalizedRotation(page);

    while (rotation > 0) {
        float temp = llx;
        llx = lly;
        lly = urx;
        urx = ury;
        ury = temp;
        rotation -= 90;
    }

    // normalize coordinates
    if (llx > urx) {
        float temp = llx;
        llx = urx;
        urx = temp;
    }

    if (lly > ury) {
        float temp = lly;
        lly = ury;
        ury = temp;
    }

    urx -= llx;
    ury -= lly;
    llx = 0;
    lly = 0;

    rect.setLowerLeftX(llx);
    rect.setLowerLeftY(lly);
    rect.setUpperRightX(urx);
    rect.setUpperRightY(ury);
    return rect;
}

/*
 *  For debugging
 */
void printRect(PDRectangle rect) {
    float llx = rect.getLowerLeftX();
    float lly = rect.getLowerLeftY();
    float urx = rect.getUpperRightX();
    float ury = rect.getUpperRightY();
    System.out.println(" llx=" + llx +
                       " lly=" + lly +
                       " urx=" + urx +
                       " ury=" + ury);
}

/*
 *  Bates-stamp a PDF file
 *
 *  Current version of PDFBox (1.8.1 as of this writing) has an incomplete
 *  conforming parser; instead it has the original (sequential) parser and
 *  a non-sequential parser workaround (added as of 1.7).  This is going
 *  to have to stick around for now, and possibly until PDFBox 2
 *
 *  Apply stamp to top left of page by default
 *  Save output in output/[index].pdf
 *  Try to create output/ if it does not already exist
 */
void stampFile(String sourcepdf, int index)
    throws IOException, COSVisitorException {
  String sourcepdfname = basename(sourcepdf);
  String targetpdf = this.OUTPUTDIR + "/" + index + ".pdf";

  PDDocument reader = null;
  try {
      reader = PDDocument.loadNonSeq(new File(sourcepdf), null);
  } catch (IOException e) {
      try {
          reader = PDDocument.load(sourcepdf);
      } catch (IOException e2) {
          System.out.println(e2);
          dieBadPDF(sourcepdfname);
      }
  }

  this.TotalFilePages = reader.getNumberOfPages();

  @SuppressWarnings("unchecked")
  List<PDPage> allpages = reader.getDocumentCatalog().getAllPages();

  // Stamp pages
  PDDocument writer = new PDDocument();
  for (int i = 1; i <= this.TotalFilePages; i++) {

      // Note: page numbers are 1-based, while lists are 0-based
      this.PageInFile = i;
      PDPage readerPage = allpages.get(i-1);

      // Calculate page zoom adjustment
      Float scale = getPageScale(readerPage);

      // Add top/bottom page margin if requested
      if (AddMargin) {
          resizePage(readerPage, scale);
      }

      // Copy source page to output document
      PDPage page = (PDPage)writer.importPage(readerPage);
      wrapInSaveRestore(page);

      PDPageContentStream canvas = new PDPageContentStream(writer, page, true, false);

      // Transform page coordinate space to match final rotated page
      PDRectangle bboxOrig = getBoundingBox(page);		// unrotated
      PDRectangle bboxFinal = getRotatedBoundingBox(page);	// rotated
      int rotation = findNormalizedRotation(page);

      Float llx = bboxOrig.getLowerLeftX();
      Float lly = bboxOrig.getLowerLeftY();

      AffineTransform m = new AffineTransform();
      // translate origin to page lower left
      m.translate(llx, lly);
      if (rotation != 0) {
          // rotate around (lower-left) origin
          int numquadrants = rotation / 90;
          m.quadrantRotate(numquadrants, 0, 0);
          // translate origin to (rotated) page lower-left
          switch (rotation) {
              case 90:
                  m.translate(0, -bboxOrig.getWidth());
                  break;
              case 180:
                  m.translate(-bboxOrig.getWidth(), -bboxOrig.getHeight());
                  break;
              case 270:
                  m.translate(-bboxOrig.getHeight(), 0);
                  break;
          }
      }

      double[] flatmatrix = new double[6];
      m.getMatrix(flatmatrix);
      canvas.concatenate2CTM(
          flatmatrix[0],
          flatmatrix[1],
          flatmatrix[2],
          flatmatrix[3],
          flatmatrix[4],
          flatmatrix[5]);

      // drawCrossBox(canvas, bboxFinal, Color.RED);	// DEBUGGING

      // PDRectangle origbox = origSizes.get(i-1);	// DEBUGGING
      // drawCrossBox(canvas, origbox, Color.GREEN);	// DEBUGGING

      // PDRectangle testbox = new PDRectangle(100,200);	// DEBUGGING
      // drawCrossBox(canvas, testbox);			// DEBUGGING

      String stamptext = addBatesStamp(canvas, bboxFinal, scale);
      canvas.close();

      // Record stamp from the first page
      if (i == 1) {
          writeStampLog(stamptext);
      }
      this.PageInCollection += 1; 
  }

  writer.save(targetpdf);

  reader.close();
  writer.close();
}

int findNormalizedRotation(PDPage page) {
    int rotation = page.findRotation();
    while (rotation < 0) {
        rotation += 360;
    }
    while (rotation >= 360) {
        rotation -= 360;
    }
    return rotation;
}

String basename(String path) throws IndexOutOfBoundsException {
    int pathend = path.lastIndexOf('/');
    String result = path;
    if (pathend != -1) {
        pathend += 1;
        result = path.substring(pathend);
    }
    return result;
}

/*
 *  For debugging - draw box using default orientation
 */
void drawBox(PDPageContentStream canvas, PDRectangle rect)
    throws IOException {

    // Draw box in current stroking color
    canvas.addRect(rect.getLowerLeftX(), rect.getLowerLeftY(),
                   rect.getWidth(), rect.getHeight());

/*
    // Draw box with sides of different colors
    float llx = rect.getLowerLeftX();
    float lly = rect.getLowerLeftY();
    float urx = rect.getUpperRightX();
    float ury = rect.getUpperRightY();
    canvas.saveGraphicsState();
    canvas.setLineWidth(8);
    canvas.moveTo(llx,ury);
    // top - red
    canvas.setStrokingColor(Color.RED);
    canvas.lineTo(urx,ury);
    canvas.stroke();
    // right - green
    canvas.setStrokingColor(Color.GREEN);
    canvas.moveTo(urx,ury);
    canvas.lineTo(urx,lly);
    canvas.stroke();
    // bottom - blue
    canvas.setStrokingColor(Color.BLUE);
    canvas.moveTo(urx,lly);
    canvas.lineTo(llx,lly);
    canvas.stroke();
    // left - orange
    canvas.setStrokingColor(Color.ORANGE);
    canvas.moveTo(llx,lly);
    canvas.lineTo(llx,ury);
    canvas.stroke();
*/

    canvas.restoreGraphicsState();
}

void drawDebugText(PDPageContentStream canvas, String text)
    throws IOException {
    canvas.saveGraphicsState();
    canvas.setNonStrokingColor(Color.BLACK);
    canvas.setFont(this.StampFont, this.StampFontSize);
    canvas.beginText();
    canvas.moveTextPositionByAmount(20,20);
    canvas.drawString(text);
    canvas.endText();
    canvas.stroke();
    canvas.restoreGraphicsState();
}

/*
 *  For debugging - draw X over box using default orientation
 */
void drawCrossBox(PDPageContentStream canvas, PDRectangle box)
    throws IOException {
      float llx = box.getLowerLeftX();
      float lly = box.getLowerLeftY();
      float urx = box.getUpperRightX();
      float ury = box.getUpperRightY();
      canvas.moveTo(llx,lly);
      canvas.lineTo(urx,ury);
      canvas.moveTo(llx,ury);
      canvas.lineTo(urx,lly);
      canvas.stroke();
      drawBox(canvas, box);
}

/*
 *  For debugging - draw X over box using default orientation with color
 */
void drawCrossBox(PDPageContentStream canvas, PDRectangle box, Color color)
    throws IOException {
      canvas.saveGraphicsState();
      canvas.setStrokingColor(color);
      drawCrossBox(canvas, box);
      canvas.restoreGraphicsState();
}

/*
 *  For debugging - draw X using default orientation with color
 */
void drawCross(PDPageContentStream canvas, PDRectangle box, Color color)
    throws IOException {
      canvas.saveGraphicsState();
      canvas.setStrokingColor(color);
      float llx = box.getLowerLeftX();
      float lly = box.getLowerLeftY();
      float urx = box.getUpperRightX();
      float ury = box.getUpperRightY();
      canvas.moveTo(llx,lly);
      canvas.lineTo(urx,ury);
      canvas.moveTo(llx,ury);
      canvas.lineTo(urx,lly);
      canvas.stroke();
      canvas.restoreGraphicsState();
}

/*
 *  For debugging - draw the rect
 */
void drawRect(PDPageContentStream ct, PDRectangle rect)
    throws IOException {
    ct.saveGraphicsState();
    ct.setStrokingColor(Color.BLACK);
    ct.addRect(rect.getLowerLeftX(), rect.getLowerLeftY(),
                 rect.getWidth(), rect.getHeight());
    ct.stroke();
    ct.restoreGraphicsState();
}

/*
 *  Compute scale adjustment for a page (for abnormally large/small pages)
 *  This is also used to adjust font sizes
 *  This function does not actually alter any PDF data
 *
 *  Most pages are OK at 1.0 but pages significantly wider than 11"
 *  or narrower than 8.5" will need stamps to be scaled up or down
 */
private Float getPageScale(PDPage page) {

    float scale = 1;
    PDRectangle pagesize = getRotatedBoundingBox(page);
    float pagewidth = pagesize.getWidth();

    if (pagewidth < LIMIT_NARROW) {
        scale = pagewidth / LIMIT_NARROW;
    } else if (pagewidth > LIMIT_WIDE) {
        scale = pagewidth / LIMIT_WIDE;
    } else {
        scale = 1;
    }
    return new Float(scale);
}

/*
 *  Expand the page to accomodate the stamp (without overwriting any
 *  of the original page data)
 *
 *  This expands the PDF media box and crop box.  This function may need
 *  to be enhanced to expand the bleed and trim boxes as well, but since
 *  those are specifically defined for physical media it's not clear that
 *  they are required.
 *
 *  The source document is altered so that each page is slightly larger.
 *  When stampFile() copies pages, it will use the altered page sizes.
 *  Technically, this does not change the page data, so page annotations
 *  (e.g. hyperlinks) are not discarded.
 *
 *  Note some PDFs apparently reuse the same mediabox dictionary entry for
 *  all pages, so make sure to create new structs when assigning new values
 */
private void resizePage(PDPage page, Float scale) {

    float mymargin = MARGIN * scale;

    int rotation = findNormalizedRotation(page);
    PDRectangle rect = getBoundingBox(page);
    if (PositionTop) {
        // Add margin to top of page
        switch (rotation) {
        case 0:
            rect.setUpperRightY(rect.getUpperRightY() + mymargin);
            break;
        case 90:
            rect.setLowerLeftX(rect.getLowerLeftX() - mymargin);
            break;
        case 180:
            rect.setLowerLeftY(rect.getLowerLeftY() - mymargin);
            break;
        case 270:
            rect.setUpperRightX(rect.getUpperRightX() + mymargin);
            break;
        default:
            die("Bad page rotation (" + rotation + ") on page " + this.PageInFile);
        }
    } else {
        // Add margin to bottom of page
        switch (rotation) {
        case 0:
            rect.setLowerLeftY(rect.getLowerLeftY() - mymargin);
            break;
        case 90:
            rect.setUpperRightX(rect.getUpperRightX() + mymargin);
            break;
        case 180:
            rect.setUpperRightY(rect.getUpperRightY() + mymargin);
            break;
        case 270:
            rect.setLowerLeftX(rect.getLowerLeftX() - mymargin);
            break;
        default:
            die("Bad page rotation (" + rotation + ") on page " + this.PageInFile);
        }
    }

    page.setCropBox(rect);
    page.setMediaBox(rect);
}

/*
 *  Stamp a page
 *
 *  Returns text of stamp
 *
 *  canvas - the writable PDF content
 *  bbox - the unrotated boundaries of the page media box
 *  scale - scaling factor for stamp position and contents (normally 1.0)
 */
private String addBatesStamp(PDPageContentStream canvas, PDRectangle bbox, float scale) throws IOException {
    float newx = bbox.getLowerLeftX() + (MARGIN * scale);
    float newy = bbox.getUpperRightY() + ((MARGINLEADING - MARGIN) * scale);

    // If StaticTime is not set, regenerate timestamp for each page
    // (possibly necessary if format includes seconds)
    if (this.StaticTime == null) {
        Date now = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat(this.DateFormat);
        this.DateStamp = dateformat.format(now);	// generate datestamp
    }

    String stamptext = "";
    for (String field : this.StampFormat) {
        if (field.equals("[date]")) {
            stamptext = stamptext + this.DateStamp;
        } else if (field.equals("[prefix]")) {
            stamptext = stamptext + this.Prefix;
        } else if (field.equals("[suffix]")) {
            stamptext = stamptext + this.Suffix;
        } else if (field.equals("[totalfiles]")) {
            stamptext = stamptext + String.format(this.TotalFilesFormat, this.TotalFiles);
        } else if (field.equals("[totalfilepages]")) {
            stamptext = stamptext + String.format(this.TotalFilePagesFormat, this.TotalFilePages);
        } else if (field.equals("[fileseq]")) {
            stamptext = stamptext + String.format(this.FileSeqFormat, this.FileSeq);
        } else if (field.equals("[pageinfile]")) {
            stamptext = stamptext + String.format(this.PageInFileFormat, this.PageInFile);
        } else if (field.equals("[pageincollection]")) {
            stamptext = stamptext + String.format(this.PageInCollectionFormat, this.PageInCollection);
        } else {	// literal
            stamptext = stamptext + field;
        }
    }

    if (!PositionTop) {		// PositionBottom
        newy = bbox.getLowerLeftY() + (MARGINLEADING * scale);
    }
    if (!PositionLeft) {	// PositionRight
        newx = bbox.getUpperRightX() - (MARGIN * scale) ;
        float linewidth = this.StampFont.getStringWidth(stamptext) / 1000 * this.StampFontSize * scale;
        newx -= linewidth;
    }

    canvas.saveGraphicsState();
    canvas.setNonStrokingColor(Color.BLACK);
    canvas.setFont(this.StampFont, this.StampFontSize * scale);
    canvas.beginText();
    canvas.moveTextPositionByAmount(newx, newy);
    canvas.drawString(stamptext);
    canvas.endText();
    canvas.stroke();
    canvas.restoreGraphicsState();

    return stamptext;
}

    /*
     *  Modify the supplied page so that its COSStream is wrapped in
     *  saveState/restoreState (q/Q) operators.
     *
     *  Copied from LayerUtility.java
     */
    public void wrapInSaveRestore(PDPage page) throws IOException
    {
        // Make a COSStream containing the 'save' operator
        COSDictionary saveGraphicsStateDic = new COSDictionary();
        COSStream saveGraphicsStateStream = new COSStream(saveGraphicsStateDic, new RandomAccessBuffer());
        OutputStream saveStream = saveGraphicsStateStream.createUnfilteredStream();
        saveStream.write("q\n".getBytes("ISO-8859-1"));
        saveStream.flush();

        // Make a COSStream containing the 'restore' operator
        COSStream restoreGraphicsStateStream = new COSStream(saveGraphicsStateDic, new RandomAccessBuffer());
        OutputStream restoreStream = restoreGraphicsStateStream.createUnfilteredStream();
        restoreStream.write("Q\n".getBytes("ISO-8859-1"));
        restoreStream.flush();

        // Read the page COSStream from the page dictionary
        // Wrap the page content in a save/restore pair (q/Q)
        // Write the page COSStream back to the page dictionary
        COSDictionary pageDictionary = page.getCOSDictionary();
        COSBase contents = pageDictionary.getDictionaryObject(COSName.CONTENTS);
        if (contents instanceof COSStream)
        {
            COSStream contentsStream = (COSStream)contents;

            COSArray array = new COSArray();
            array.add(saveGraphicsStateStream);
            array.add(contentsStream);
            array.add(restoreGraphicsStateStream);

            pageDictionary.setItem(COSName.CONTENTS, array);
        }
        else if( contents instanceof COSArray )
        {
            COSArray contentsArray = (COSArray)contents;

            contentsArray.add(0, saveGraphicsStateStream);
            contentsArray.add(restoreGraphicsStateStream);
        }
        else if( contents == null )
        {
            COSArray array = new COSArray();
            array.add(saveGraphicsStateStream);
            array.add(restoreGraphicsStateStream);

            pageDictionary.setItem(COSName.CONTENTS, array);
        }
        else
        {
            throw new IOException("Contents are unknown type: " + contents.getClass().getName());
        }
    }

public static void usage() {
    System.err.println("Usage: java -jar Stamper.jar jobfile");
}

public static void die(String msg) {
    System.err.println(msg);
    System.exit(1);
}

public static void dieBadPDF(String pdfname) {
    System.err.println("Invalid PDF: " + pdfname);
    System.exit(2);
}

/*
 *  Read configs and file names from the job file
 *
 *  NOTE do not unconditionally trim(), config specifiers may have 
 *  significant whitespace (esp. Prefix/Suffix)
 */
private void processJobFile(String filename) throws FileNotFoundException, IOException {
    BufferedReader in = new BufferedReader(new FileReader(filename));
    String line = "";
    while ((line = in.readLine()) != null) {
        if (line.startsWith("#")) {
            continue;
        }
        if (line.equals("::")) {
            break;
        }
        String[] parts = line.split(":", 2);
        if (parts.length != 2) {
            die("Bad config: [" + line + "]");
        }
        String configname = parts[0].trim();
        String configval = parts[1];
        if (configname.equals("location")) {
            if (configval.equals("bottomleft")) {
                this.PositionLeft = true;
                this.PositionTop = false;
            } else if (configval.equals("bottomright")) {
                this.PositionLeft = false;
                this.PositionTop = false;
            } else if (configval.equals("topleft")) {
                this.PositionLeft = true;
                this.PositionTop = true;
            } else if (configval.equals("topright")) {
                this.PositionLeft = false;
                this.PositionTop = true;
            } else {
                die("Bad location [" + configval + "]");
            }
        } else if (configname.equals("stampfontsize")) {
            this.StampFontSize = Integer.valueOf(configval).intValue();
        } else if (configname.equals("prefix")) {
            this.Prefix = configval;
        } else if (configname.equals("suffix")) {
            this.Suffix = configval;
        } else if (configname.equals("totalfiles")) {
            this.TotalFiles = Integer.valueOf(configval).intValue();
        } else if (configname.equals("startfileseq")) {
            this.FileSeq = Integer.valueOf(configval).intValue();
        } else if (configname.equals("startpageincollection")) {
            this.PageInCollection = Integer.valueOf(configval).intValue();
        } else if (configname.equals("addmargin")) {
            if (configval.equals("true")) {
                this.AddMargin = true;
            } else if (configval.equals("false")) {
                this.AddMargin = false;
            } else {
                die("Bad AddMargin setting [" + configval + "]");
            }
        } else if (configname.equals("stampformat")) {
            this.StampFormatOrig = configval;
            // Note: does not handle escaped bracket delimiters
            // Tokens are bracketed strings from below list (e.g. [date])
            String[] tokenlist = {
                "date",
                "prefix",
                "suffix",
                "totalfiles",
                "totalfilepages",
                "fileseq",
                "pageinfile",
                "pageincollection"
            };
            String regex = "";
            for (String token : tokenlist) {
                // Make lookahead/lookbehind regex patterns for all tokens
                String token_behind = "(?<=\\[" + token + "\\])";
                String token_ahead = "(?=\\[" + token + "\\])";
                if (regex.length() != 0) {
                    regex = regex + "|";
                }
                regex = regex + token_behind + "|" + token_ahead;
            }
            this.StampFormat = configval.split(regex);

        } else if (configname.equals("startpageincollection")) {
            this.PageInCollection = Integer.valueOf(configval).intValue();
        } else if (configname.equals("statictime")) {
            this.StaticTime = new Date(Long.parseLong(configval));
        } else if (configname.equals("dateformat")) {
            // Remove escaped quotes ''
            String testval = configval.replace("''", "");
            // Remove characters between (and including) single quotes
            testval = testval.replaceAll("'.*'", "");
            // Letters in dateformat must be in the set: GyMwWDdFEaHkKhmsSzZ
            String validset = "GyMwWDdFEaHkKhmsSzZ";
            for (char c : testval.toCharArray()) {
                if (Character.isLetter(c) && (validset.indexOf(c) == -1)) {
                    die("Bad date format specifier: [" + c + "]");
                }
            }
            this.DateFormat = configval;
        } else if (configname.equals("totalfilesformat")) {
            this.TotalFilesFormat = configval;
        } else if (configname.equals("totalfilepagesformat")) {
            this.TotalFilePagesFormat = configval;
        } else if (configname.equals("fileseqformat")) {
            this.FileSeqFormat = configval;
        } else if (configname.equals("pageinfileformat")) {
            this.PageInFileFormat = configval;
        } else if (configname.equals("pageincollectionformat")) {
            this.PageInCollectionFormat = configval;
        } else {
            die("Unrecognized config option: [" + configname + "]");
        }
    }

    // Add files
    filelist = new ArrayList<String>();
    while ((line = in.readLine()) != null) {
        if (line.startsWith("#")) {
            continue;
        }
        filelist.add(line);
    }
    if (this.TotalFiles == 0) {
        this.TotalFiles = filelist.size();
    }

    // If the job spec includes a static time, use that timestamp
    // for all pages (e.g. don't change date over midnight)
    if (this.StaticTime != null) {
        Date now = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat(this.DateFormat);
        this.DateStamp = dateformat.format(now);	// generate datestamp
    }
}

/*
 *  Write first page stamp to output log (the first stamp from every document
 *  is included as part of the final report Excel spreadsheet)
 */
public void writeStampLog(String rec) throws IOException {
    String filename = this.OUTPUTDIR + "/" + this.STAMPLOG;
    PrintWriter out = new PrintWriter(new FileWriter(filename, true)); // append
    out.print(rec + "\n");
    out.close();
}

/*
 *  Write new job file for continuation in output/nextjob.conf
 */
public void writeJobFile() throws FileNotFoundException, IOException {
    String filename = this.OUTPUTDIR + "/" + this.NEXTCONF;

    PrintWriter out = new PrintWriter(new FileWriter(filename));
    if (this.PositionTop && this.PositionLeft) {
        out.print("location:topleft\n");
    } else if (this.PositionTop && !this.PositionLeft) {
        out.print("location:topright\n");
    } else if (!this.PositionTop && this.PositionLeft) {
        out.print("location:bottomleft\n");
    } else if (!this.PositionTop && !this.PositionLeft) {
        out.print("location:bottomright\n");
    }

    out.print("stampfontsize:" + this.StampFontSize + "\n");

    if (this.AddMargin) {
        out.print("addmargin:true\n");
    } else {
        out.print("addmargin:false\n");
    }

    out.print("prefix:" + this.Prefix + "\n");
    out.print("suffix:" + this.Suffix + "\n");
    out.print("totalfiles:" + this.TotalFiles + "\n");
    out.print("startfileseq:" + this.FileSeq + "\n");
    out.print("startpageincollection:" + String.format(this.PageInCollectionFormat, this.PageInCollection) + "\n");
    out.print("stampformat:" + this.StampFormatOrig + "\n");

    out.print("totalfilesformat:" + this.TotalFilesFormat + "\n");
    out.print("totalfilepagesformat:" + this.TotalFilePagesFormat + "\n");
    out.print("fileseqformat:" + this.FileSeqFormat + "\n");
    out.print("pageinfileformat:" + this.PageInFileFormat + "\n");
    out.print("pageincollectionformat:" + this.PageInCollectionFormat + "\n");

    out.print("dateformat:" + this.DateFormat + "\n");
    if (this.StaticTime != null) {
        out.print("statictime:" + this.StaticTime.getTime() + "\n");
    }
    out.print("::\n");
    out.close();
}

/*
 * java -jar Stamper.jar jobfile
 * (make sure -cp includes itext)
 */
public static void main(String[] args) throws IOException {
    Stamper app = new Stamper();
    if (args.length != 1) {
        usage();
        System.exit(1);
    }
    String jobfile = args[0];

    app.processJobFile(jobfile);	// read job options

    app.sanityCheck();

    app.MARGIN = app.StampFontSize * 3;
    app.MARGINLEADING = app.StampFontSize;

    int idx = 1;
    for (String filename : app.filelist) {
        System.out.println(filename);
        try {
            app.stampFile(filename, idx);
            System.out.println("Stamping complete");
        } catch (IOException e) {
            die("IOException processing " + filename + ": " + e.getMessage());
        } catch (COSVisitorException e) {
            die("COSVisitorException processing " + filename + ": " + e.getMessage());
        }
        app.FileSeq += 1;
        idx += 1;
    }
    // if we completed with no errors, write the updated job file
    app.writeJobFile();
}

}
