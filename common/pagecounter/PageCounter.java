import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

class PageCounter {

int PDFPageCount(String path) throws IOException {
   PDDocument document = PDDocument.load(path);
   PDDocumentInformation info = document.getDocumentInformation();
   return document.getNumberOfPages();
}

public static void usage() {
    System.err.println("Usage: PageCounter <pdffile>");
}

/*
 * java -jar PageCounter.jar filename
 * (make sure -cp includes itext)
 */
public static void main(String[] args) throws IOException {
    PageCounter counter = new PageCounter();
    if (args.length != 1) {
        usage();
        System.exit(1);
    }
    String filename = args[0];
    try {
        int pagecount = counter.PDFPageCount(filename);
        System.out.println(pagecount);
    } catch (IOException e) {
        System.err.println("Error processing " + filename + ": " + e.getMessage());
        System.exit(1);
    }
}

}
