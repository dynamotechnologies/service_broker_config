<?php

#
#  Parse an ini file in the format used by the Python ConfigParser
#  Note that parse_ini_file() will not work because it uses semicolons for
#  comments (instead of hashes) and equals for assignment (instead of colons)
#
#  Example:
#
#  $configarray = parseConfigSection("/var/www/config/dmUtil-conf",
#                                    "GOVDELIVERY");
#  define("GOVDELIVERY_PASSWORD", $configarray["PASSWORD"]);
#
function parseConfigSection($file, $sectionname) {
    # Open file
    $config = file($file);
    
    # Find section
    $configs = array();
    $goodsection = false;
    foreach ($config as $idx => $line) {
        $line = trim($line);
        if (strlen($line) == 0) { continue; }
        if ($line[0] == "#") { continue; }
        if ($line[0] == "[") {
            $section = preg_replace("/\[(.*)\]/", "$1", $line);
            if ($sectionname && ($section == $sectionname)) {
                $goodsection = true;
            } else {
                $goodsection = false;
            }
            continue;
        }
        if ($goodsection) {
            # Parse configs
            $parts = preg_split("/ *: */", $line);
            if (count($parts) == 2) {
            $key = $parts[0];
            $valueparts = preg_split("/[^\\\];.*/", $parts[1]);
            $value = trim($valueparts[0]);
            $configs[$key] = $value;
            } else {
                continue;	// Bad config
            }
        }
    }
    
    # Return array
    return $configs;
}

#
#   Get USER:PASSWORD for GovDelivery API
#
function get_govdelivery_userpwd() {
    $gd_conf = parseConfigSection("/var/www/config/dmUtil-conf", "GOVDELIVERY");
    $gd_uid = $gd_conf['USERNAME'];
    $gd_pwd = $gd_conf['PASSWORD'];
    return "$gd_uid:$gd_pwd";
}

?>
