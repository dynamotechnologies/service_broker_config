#!/usr/bin/python3

import sys
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import configparser
from urllib.error import HTTPError

testmode = False

#
#	Python library for accessing HTTP services (Datamart, GovDelivery)
#
#	The name "dmUtil" is a historical quirk because this was originally
#	written to access Datamart services.
#
#	Call initOpener() immediately after importing the library
#
#	Available functions are:
#	post(url, xml)
#	put(url, xml)
#	merge(baseurl, id, xml)	(first try a PUT, then fall back to a POST)
#	delete(url)
#	get(url)
#
#	Call useTestMode() to deactivate all functions other than HTTP get
#

def useTestMode():
	testmode = True

#
#	Configure opener to handle HTTP BASIC authentication by default
#
def initOpener():
	password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()

	# Add server credentials from /var/www/config/dmUtil-conf

	myConf = configparser.SafeConfigParser()
	myConf.optionxform = str
	myConf.read('/var/www/config/dmUtil-conf')
	configs = {}

	###########################
	# Access to Datamart server
	###########################
	configs.update(myConf.items("DATAMART"))
	top_level_url = configs['TOP_LEVEL_URL']
	username = configs['USERNAME']
	password = configs['PASSWORD']
	password_mgr.add_password(None, top_level_url, username, password)

	##############################
	# Access to GovDelivery server
	##############################
	configs.update(myConf.items("GOVDELIVERY"))
	top_level_url = configs['TOP_LEVEL_URL']
	username = configs['USERNAME']
	password = configs['PASSWORD']
	password_mgr.add_password(None, top_level_url, username, password)

	handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
	opener = urllib.request.build_opener(handler)

	# Install the opener.
	# Now all calls to urllib2.urlopen use our opener.
	urllib.request.install_opener(opener)

def post(url, xml):
	if (testmode):
		return
	request = urllib.request.Request(url, xml.strip())
	request.add_header('Content-Type', 'application/xml')

	try:
		response = urllib.request.urlopen(request)
		return response.read()
	except HTTPError as e:
		if (e.code == 201):
			return "201 Created OK"
		else:
			raise e

def put(url, xml):
	if (testmode):
		return
	request = urllib.request.Request(url, xml.strip())
	request.add_header('Content-Type', 'application/xml')
	request.get_method = lambda: 'PUT'

	response = urllib.request.urlopen(request)
	return response.read()

def merge(baseurl, id, xml):
	if (testmode):
		return
	try:
		return put(baseurl + '/' + id, xml)
	except HTTPError as e:
		if (e.code == 404):
			return post(baseurl, xml)
		else:
			raise e

def delete(url):
	if (testmode):
		return
	request = urllib.request.Request(url, None)
	request.get_method = lambda: 'DELETE'

	try:
		response = urllib.request.urlopen(request)
		return response.read()
	except HTTPError as e:
		if (e.code == 204):
			return "204 No data OK"
		else:
			raise e

def get(url):
	print(url)
	request = urllib.request.Request(url)
	response = urllib.request.urlopen(request)
	return response.read()
