<?php
/*
 *	Install this file as /var/www/config/uploader-conf.php
 */

/*
 *	Set hostname for filerecorder service
 */
define("FILERECORDERHOST", 'localhost');

/*
 *	Set location of service broker (localhost)
 */
// define("BROKERDIR", '/var/www/html/drmdev_mth');	# DEV
// define("BROKERDIR", '/var/www/html/drmtest');	# CORETST / PHE-PRP
define("BROKERDIR", '/var/www/html/dmdpilot');	# PRODUCTION

// define("BROKERUSER", '');	# DEV
// define("BROKERPASS", '');	# DEV
define("BROKERUSER", '___');	# CORETST / PRODUCTION
define("BROKERPASS", '___');	# CORETST / PRODUCTION

/*
 *	Set URL to notify PALS of new files
 */
// define("UPDATEPALSURL", 'http://svcbroker.fstst.phaseonecg.com/drmtest/testalert.php');	# DEV
// define("UPDATEPALSURL", 'http://www.nsgi-hq.com/pals/jobStatusAction.do');			# TEST_NSGI
// define("UPDATEPALSURL", 'http://slpheoras022.phe.fs.fed.us/pals/jobStatusAction.do');	# TEST_PHE
// define("UPDATEPALSURL", 'http://apps.prp.fs.fed.us/pals/jobStatusAction.do');		# PREPRODUCTION_PRP
define("UPDATEPALSURL", 'http://apps.fs.fed.us/pals/jobStatusAction.do');			# PRODUCTION

/*
 *	Set prefix for Datamart access
 */
// define ("DATAMART_PREFIX", 'http://localhost:7001/api/1_0');	# DEV
// define ("DATAMART_PREFIX", 'http://localhost:7001/api/1_0');	# TEST
define ("DATAMART_PREFIX", 'http://localhost:8080/api/1_0');	# PILOT

/*
 *	Set auth for Datamart access
 */
//define ("DATAMART_AUTH", 'svcbroker:Br0k3r3squ3');		# PILOT
define ("DATAMART_AUTH", '______');	# TEST

/*
 *      Set ID for Google Analytics tracking
 *      (leave undefined for development)
 */
define ("GA_TRACKER_ID", 'UA-2172917-11');   # CORETST
//define ("GA_TRACKER_ID", 'UA-2172917-12');   # PILOT

?>
