<?php

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 			CONFIGURATION: FILL IN THE BLANKS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/*
 * Path to Broker proxy
 * --------------------
 * CoreTst2:	/var/www/html/drmtest/proxies/drmProxy.php
 * SvcBkrProd2:	/var/www/html/dmdpilot/proxies/drmProxy.php
 *
*/
require_once("/var/www/html/dmdpilot/proxies/drmProxy.php");

/*
 * Instances
 * ---------
 * CoreTst2:	coretest2
 * SvcBkrProd2:	coreprod2
 *
*/
$INSTANCENAME = "coreprod2";


/*
 * Test Files
 * ----------
 * CoreTst2:	FSTST_000001
 * SvcBkrProd2:	FSTST_000001
 *
*/
$FILEID = "FSTST_000001";	# ID of a file to retrieve...must be known in advance


/*
 * Broker HTTP Path
 * ----------------
 * CoreTst2:	http://br0k3r:s3rv1c3@127.0.0.1/drmtest/broker/broker.php
 * SvcBkrProd2:	http://br0k3r:s3rv1c3@127.0.0.1/dmdpilot/broker/broker.php
 *
*/
$BROKER_URL = "http://br0k3r:s3rv1c3@127.0.0.1/dmdpilot/broker/monitor.php";


/*
 * UCM HTTP Path
 * -------------
 * CoreTst2:    $UCM_URL = "http://sysadmin:p1cgorcl@oracleprod1/web/idcplg";
 * SvcBkrProd2: $UCM_URL = "http://sysadmin:p1cgorcl@oracleprod2/web/idcplg";
*/
$UCM_URL = "http://sysadmin:p1cgorcl@oracleprod2/web/idcplg";


/*
 * Refinery HTTP Path
 * ------------------
 * CoreTst2:	http://refadmin:p1cgorcl@ec2-54-235-157-223.compute-1.amazonaws.com/web/idcplg
 * SvcBkrProd2:	http://refadmin:p1cgorcl@ec2-54-235-157-223.compute-1.amazonaws.com/web/idcplg
 *
*/
//$REF_URL = "http://refadmin:p1cgorcl@ec2-54-235-157-223.compute-1.amazonaws.com/web/idcplg";
//$REF_URL = "http://refadmin:p1cgorcl@ec2-54-221-61-164.compute-1.amazonaws.com/web/idcplg";
//$REF_URL = "http://refadmin:p1cgorcl@ec2-54-91-88-153.compute-1.amazonaws.com/web/idcplg";
$REF_URL = "http://sysadmin:p1cgorcl@oracleprod2/web/idcplg";

/*
 * Legacy mode uses the old format, which among other things assumes all 
 * 	processes are running on the same server. Use Legacy mode for 
 * 	INETU and the pocg test/dev boxes only.
 *
*/
$LEGACY_MODE=false;


// How long to wait before complaining about a failed conversion
$TIMEOUT_INTERVAL = "30 minute";

/*
 *  Location of SOLR master server:
 *  coretst2:  $SOLR_MASTER= "oracletest2";
 *  coreprod2: $SOLR_MASTER= "oracleprod2";
 */
$SOLR_MASTER= "oracleprod2";





// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//			END OF CONFIG SECTION
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~







/*
 *	START OF TESTS
 */
$output;

if ($LEGACY_MODE) {
  $PID_STEL = file_get_contents("/stellent/$INSTANCENAME/etc/pid");
  if (strlen($PID_STEL)>0) {
	$PID_STEL = strlen(exec("ps -A | grep " . $PID_STEL . ".*java"))>0 ? 1 : 0;
  } else {
	$PID_STEL=0;
  }
  $PID_STELADMIN = file_get_contents("/stellent/$INSTANCENAME/admin/etc/pid");
  if (strlen($PID_STELADMIN)>0) {
	$PID_STELADMIN = strlen(exec("ps -A | grep " . $PID_STELADMIN . ".*java"))>0 ? 1 : 0;
  } else {
	$PID_STELADMIN=0;
  }
  $PID_REFINERY = strlen(exec("ps -A | grep " . $PID_REFINERY . ".*java"))>0 ? 1 : 0;
  $PID_OO = strlen(exec("ps -A | grep " . $PID_OO . ".*soffice.bin"))>0 ? 1 : 0;
  $PID_XVFB = strlen(exec("ps -A | grep " . $PID_XVFB . ".*Xvfb"))>0 ? 1 : 0;
} else {
  $PID_STEL = file_get_contents($UCM_URL."?IdcService=PING_SERVER&IsJava=1");
  $PID_STEL = (strpos($PID_STEL,"StatusMessage message")) ? 1 : 0;

  $PID_REFINERY = file_get_contents($REF_URL."?IdcService=PING_SERVER&IsJava=1");
  $PID_REFINERY = (strpos($PID_REFINERY,"StatusMessage message")) ? 1 : 0;

  // Force success for legacy settings -- these are no longer needed
  $PID_STELADMIN = 1;
  $PID_OO = 1;
  $PID_XVFB = 1;
}
ob_start();
$echoStatus = testEcho($BROKER_URL);
$echoMsgs = ob_get_contents();
ob_end_clean();
ob_start();
$getFileDataStatus = testGetFileData($BROKER_URL, $FILEID);
# $getFileDataStatus = 1;
# print "Test temporarily disabled--system error";
$getFileDataMsgs = ob_get_contents();
ob_end_clean();
ob_start();
$conversionStatus = testGetStatusList($TIMEOUT_INTERVAL);
$conversionMsgs = ob_get_contents();
ob_end_clean();
ob_start();
$schedulerStatus = testScheduler($BROKER_URL);
$schedulerMsgs = ob_get_contents();
ob_end_clean();
ob_start();
$solrStatus = testSolr();
$solrMsgs = ob_get_contents();
ob_end_clean();
ob_start();
$solrMasterStatus = testSolrMaster();
$solrMasterMsgs = ob_get_contents();
ob_end_clean();

$badfilelist = glob("/usr/tmp/debuglog/*badrequest.debug");
$badBrokerRequestCount = count($badfilelist);
$badBrokerRequestStatus = 1;
$badBrokerNonEmptyRequestCount = 0;
foreach ($badfilelist as $badfile) {
    if (filesize($badfile) > 0) {
        $badBrokerNonEmptyRequestCount += 1;
    }
}
if ($badBrokerNonEmptyRequestCount > 100) {
    $badBrokerRequestStatus = 0;
}

if (
  $PID_REFINERY==1 &&
  $PID_OO==1 &&
  $PID_XVFB==1 &&
  $PID_STEL==1 &&
  $PID_STELADMIN==1 &&
  $echoStatus==1 &&
  $getFileDataStatus==1 &&
  $schedulerStatus==1 &&
  $solrStatus==1 &&
  $badBrokerRequestStatus==1 &&
  $solrMasterStatus==1 &&
  $conversionStatus==1
) {
	echo "<H1>Everything is functioning OK!</H1>";
} else {
	echo "<H1>We Have Problems!</H1>";
}

//Checking state of server. If it is 0 email will be sent.
if ($PID_REFINERY==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'REFINERY is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }

  if($PID_OO==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Service Down';
$message = 'OPENOFFICE is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($PID_XVFB==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'XVFB is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($PID_STEL==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'STELLENT is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($PID_STELADMIN==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'STELLENT ADMIN is down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($echoStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Broker accepting requests is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($getFileDataStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Stellent accepting requests is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($schedulerStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Broker job scheduler status is down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($solrStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Solr search engine status is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($badBrokerRequestStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Nonempty bad broker requests is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($solrMasterStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Solr master engine status is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }
  
  if($conversionStatus==0){
$to      = 'fs-emnepa-alerts@nsgi-hq.com';
$subject = 'Server Down';
$message = 'Conversions Successful is Down';
$headers = 'From: fs-emnepa-support@nsgi-hq.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

//mail($to, $subject, $message, $headers);
  }

echo "<br/><br/><br/>";

echo "<table border=\"1\" summary=\"This table shows the state of the repository processes\">";
echo "<tr>";
echo "<th>PROCESS</th><th>STATE (1=UP, 0=DOWN)</th><th>MESSAGES</th>";
echo "</tr>";
echo "<tr>";
echo "<td>STELLENT</td><td>$PID_STEL</td>";
echo "</tr>";
echo "<tr>";
echo "<td>STELLENT ADMIN</td><td>$PID_STELADMIN</td>";
echo "</tr>";
echo "<tr>";
echo "<td>REFINERY</td><td>$PID_REFINERY</td>";
echo "</tr>";
if ($LEGACY_MODE) {
echo "<tr>";
echo "<td>OPENOFFICE</td><td>$PID_OO</td>";
echo "</tr>";
echo "<tr>";
echo "<td>XVFB</td><td>$PID_XVFB</td>";
echo "</tr>";
}
echo "<tr>";
echo "<td>Broker accepting requests</td><td>$echoStatus</td>";
echo "<td><PRE>$echoMsgs</PRE></td>";
echo "</tr>";
echo "<tr>";
echo "<td>Stellent accepting requests</td><td>$getFileDataStatus</td>";
echo "<td><PRE>$getFileDataMsgs</PRE></td>";
echo "</tr>";
echo "<tr>";
echo "<td>Broker job scheduler status</td><td>$schedulerStatus</td>";
echo "<td><PRE>$schedulerMsgs</PRE></td>";
echo "</tr>";
echo "<tr>";
echo "<td>Solr search engine status</td><td>$solrStatus</td>";
echo "<td><PRE>$solrMsgs</PRE></td>";
echo "</tr>";
echo "<tr>";
echo "<td>Solr master engine status</td><td>$solrMasterStatus</td>";
echo "<td><PRE>$solrMasterMsgs</PRE></td>";
echo "</tr>";
echo "<tr>";
echo "<tr>";
echo "<td>Nonempty bad broker requests</td><td>$badBrokerRequestStatus</td>";
echo "<td><PRE>Total bad requests: $badBrokerRequestCount</PRE></td>";
echo "</tr>";
echo "<td>Conversions Successful</td><td>$conversionStatus</td>";
echo "<td><PRE>$conversionMsgs</PRE></td>";
echo "</tr>";
echo "</table>";


/*
 *	FUNCTIONS
 */

  /*
   *	Issue a string to the broker, which should echo it right back without
   *	involving the back-end server.
   */
  function testEcho($url) {

    $testVal = "hi there";	# This string will be echoed from the broker

    $xmlrpc_client = new xmlrpc_client($url);
    // $xmlrpc_client->setVerbose(True);	// DEBUGGING
    $xmlrpc_msg = xmlrpc_encode_request('echoMsg', array($testVal));

    $xmlrpc_resp = $xmlrpc_client->send($xmlrpc_msg);

    if (xmlrpc_get_type($xmlrpc_resp) == "string") {

      // Success

      if (strcmp($xmlrpc_resp, $testVal) == 0) {
        print "echoMsg succeeded\n";
        return 1;
      } else {
        print "echoMsg response incorrect!  Returned [$xmlrpc_resp]\n";
        return 0;
      }

    } else {

      // Failure

      print("echoMsg failed!!!\n");
      print "Fault code " . $xmlrpc_resp['faultCode'] . "\n";
      print "Fault message " . $xmlrpc_resp['faultString'] . "\n";

      // print_r($xmlrpc_resp);
      return 0;
    }
  }

  function testGetFileData($url, $fileID) {

    $xmlrpc_client = new xmlrpc_client($url);
    // $xmlrpc_client->setVerbose(True);	// DEBUGGING
    $xmlrpc_msg = xmlrpc_encode_request('getFileData', array($fileID));

    $xmlrpc_resp = $xmlrpc_client->send($xmlrpc_msg);

    if (xmlrpc_get_type($xmlrpc_resp) == "base64") {

      // Success
      // TBD Should check contents of file here
      // print $xmlrpc_resp->scalar;

      $file = $xmlrpc_resp->scalar;
      print "getFileData($fileID) succeeded\n";
      return 1;

    } else {

      // Failure

      print("getFileData($fileID) failed!!!\n");

      print "Fault code " . $xmlrpc_resp['faultCode'] . "\n";
      print "Fault message " . $xmlrpc_resp['faultString'] . "\n";

      // print_r($xmlrpc_resp);
      return 0;
    }
  }

  function testGetStatusList($interval) {
    $testProxy	= new DrmProxy();
    $r			= $testProxy->getStatusList();
    $numfailed	= 0;
    $err		= "";
    $strRows	= "";

	// Was a status list returned?
	//    If so, loop through it and extract key metadata for reporting
    if (isset($r->DocStatusListResult->StatusList)) {
      $doclist = $r->DocStatusListResult->StatusList;
      /* Single-item lists return inconsistently--wrap in another array */
      if (!array_key_exists(0, $doclist)) {
          $doclist = array($doclist);
      }
      foreach ($doclist as $doc) {
        $docDate = date('Y-m-d-H-i-s',strtotime($doc->dInDate));
        $timeout = date('Y-m-d-H-i-s',strtotime("now" . " -" . $interval));

		// only complain about docs older than the timeout interval
        if ($docDate<$timeout) {
          $strRows .= "<tr><td>" . $doc->dDocName . "</td><td>" . $doc->dInDate . "</td><td>" . $doc->dStatus . "</td></tr>";
		  $numfailed++;
        }
      }
      if ($numfailed>0) {
        $err .= "<strong>" . $numfailed . " documents are stuck in queue:</strong><br/><br/>";
        $err .= "<table border='1'><tr><td>dDocName</td><td>dInDate</td><td>dStatus</td></tr>";
        $err .= $strRows;
        $err .= "</table>";
	  }

	// No status list returned
    } elseif (isset($r->DocStatusListResult->StatusInfo->statusCode)) {
      // check for zero length
      var_dump($r->DocStatusListResult->StatusInfo->statusCode);

    // No feedback from server. Possibly a server timeout or non-xml response
    } else {
      $err .= "unknown error";
    }

    if (strlen($err)>0) {
      print $err;
      return 0;
    } else {
      print "testGetStatusList succeeded";
      return 1;
    }
  }


// util function for testScheduler
// to prune names of dot files
function filevisible($fname)
{
        if ($fname[0] == '.') {
                return false;
        } else {
                return true;
        }
}


function testScheduler($broker_url) {
  try {
    $urlPath = parse_url($broker_url, PHP_URL_PATH);
    $urlArray = explode('/', $urlPath);
    $instance = $urlArray[1];
    $tmpDir = "/var/www/html/${instance}/tmpdir";
    $inQDir = "/var/www/html/${instance}/job_queues/in_q";
    $logDir = "/var/www/html/${instance}/logs";
    $schedulerLockFile = "${tmpDir}/lock.scheduler";
    $schedulerPidFile = "${tmpDir}/pid.scheduler";
  } catch (Exception $e) {
    print "Unable to find job control files!\n";
    return 0;
  }

  $lockinfo = @stat($schedulerLockFile);
  if (!$lockinfo) {
    print "No lockfile present!\n";
    return 0;
  }

  $pidinfo = @stat($schedulerPidFile);
  if (!$pidinfo) {
    print "No PID present!\n";
    return 0;
  }

  $pid = @file_get_contents($schedulerPidFile);
  if (!$pid) {
    print "Can't get PID!\n";
    return 0;
  }
  $pid = trim($pid);

  $procinfo = "/proc/${pid}/cmdline";
  $cmd = @file_get_contents($procinfo);
  if (!$cmd) {
    print "Can't get command line from /proc/${pid}/cmdline!\n";
    return 0;
  }

  if (!strpos($cmd, "scheduler")) {
    print "PID ${pid} does not appear to be the scheduler script!\n";
    return 0;
  }

  $dirlist = scandir($inQDir);
  $dirlist = array_filter($dirlist, "filevisible");
  print "There are " . count($dirlist) . " jobs pending\n";

  $stat = stat("${logDir}/job_detail.log");
  $ago = time() - $stat['mtime'];       // time since last update, in sec
  print "Last job was handled " . $ago . " seconds ago\n";

  //job shouldn't take longer than, say, 20min to complete
  $job_time_limit = 20 * 60;
  if ((count($dirlist) > 0) && ($ago > $job_time_limit)) {
    print "Last job was longer than 20 minutes ago!  Check for errors\n";
    return 0;
  }

  print "Job scheduler is running\n";
  return 1;
}

function testSolr() {
  $str = @file_get_contents("http://localhost:8983/solr/admin/ping");
  if ($str === False) {
    print "No response from Solr, is server running?";
    return 0;
  } else {
    $xml = new SimpleXMLElement($str);
    $status = trim(strval($xml->str[0]));
    print "Response from Solr ping: $status";
    if ($status !== "OK") {
      return 0;
    }
  }
  return 1;
}

function testSolrMaster() {
  global $SOLR_MASTER;

  $masterhost = $SOLR_MASTER;
  $str = @file_get_contents("http://${masterhost}:8983/solr/admin/ping");
  if ($str === False) {
    print "No response from Solr on ${masterhost}, is master server running?";
    return 0;
  } else {
    $xml = new SimpleXMLElement($str);
    $status = trim(strval($xml->str[0]));
    print "Response from Solr master ping: $status";
    if ($status !== "OK") {
      return 0;
    }
  }
  return 1;
}


class xmlrpc_client {
    private $ch;	// curl handle

    function __construct($url) {
        $this->ch=curl_init();
        curl_setopt($this->ch, CURLOPT_URL, "$url");
        // Return a variable instead of posting it directly
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        $httpheaders = array('Content-type: text/xml');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $httpheaders);

        curl_setopt($this->ch, CURLOPT_POST, True);
    }

    public function setCredentials($user, $pass) {
        $AUTH_USERPWD = utf8_encode("$user:$pass");
        curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->ch, CURLOPT_USERPWD, $AUTH_USERPWD);
    }

    // $bool should be True/False
    public function setVerbose($bool) {
        curl_setopt($this->ch, CURLOPT_VERBOSE, $bool);
    }

    public function send($msg) {
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $msg);
        $resultxml = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        $httpcode = $info["http_code"];
        curl_close($this->ch);
        // All requests (even failures) should return with a 20x code if the
        // server didn't collapse
        if (($httpcode < 200) || ($httpcode > 299)) {
            $infotxt = print_r($info, True);
            $fakeResponse = array(
                'faultString' => "Error from XMLRPC call: $infotxt",
                'faultCode' => $httpcode
            );
            return $fakeResponse;
        } else {
            $method = "";
            $result = xmlrpc_decode_request($resultxml, $method);
            return $result;
        }
    }
}

?>

