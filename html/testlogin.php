<?php require_once("lib/MLM.php");?>

<?php  
    require_once("lib/MLM.php");
    if (!( defined("ENABLE_TEST_LOGIN") && ENABLE_TEST_LOGIN === "1" )) {
        echo "Disabled"; exit(0);
    }
?><html>
<head>
<TITLE>Test login page</TITLE>
</head>
<body>
<pre>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
?>

<form action="testlogin.php" method="POST">
  User <input name="user" type="text" value="samuelrcollins"/> <input value="login" type="submit"/>
  <input name="action" type="hidden" value="login"/>
</form>
<form action="testlogin.php" method="POST">
  <input value="Sync!" type="submit"/>
  <span>Sync topics with GovDelivery</span>
  <input name="action" type="hidden" value="synctopics"/>
</form>
<form action="testlogin.php" method="POST">
  <input value="Get Subscribers!" type="submit"/>
  <span>Get new subscribers from Datamart</span>
  <input name="action" type="hidden" value="getsubscribers"/>
</form>
<form action="testlogin.php" method="POST">
  <input value="Purge Subscribers!" type="submit"/>
  <span>Delete all subscribers from list</span>
  topic id <input name="listtopicid" type="text" value="NEPA_990000_S"/>
  <input name="action" type="hidden" value="deleteallsubscribers"/>
</form>
<form action="testlogin.php" method="POST">
  <input value="New List!" type="submit"/>
  <span>Create test mailing list (that will not exist in GovDelivery)</span>
  topic id <input name="listtopicid" type="text" value="NEPA_990000_S"/>
  name     <input name="listname" type="text" value="0000 TESTNAME"/>
  unit id  <input name="listunitid" type="text" value="1100"/>
  <input name="action" type="hidden" value="addtestlist"/>
</form>
<form action="testlogin.php" method="POST">
  <input value="Delete List!" type="submit"/>
  <span>Remove test mailing list permanently</span>
  topic id <input name="listtopicid" type="text" value="NEPA_990000_S"/>
  <input name="action" type="hidden" value="deletelist"/>
</form>
<?php

} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  if ($_POST['action'] == "synctopics") {
    echo "<PRE>";
    echo "Sync request\n";
    $mlm = new MLM();
    $mlm->syncLists();
    echo "</PRE>";
  } else if ($_POST['action'] == "getsubscribers") {
    echo "<PRE>";
    echo "Get new subscribers request\n";
    $mlm = new MLM();
    $mlm->addSubscribersFromDatamart();
    echo "</PRE>";
  } else if ($_POST['action'] == "login" && isset($_POST['user'])) {
    $user = $_POST['user'];
    $pass = md5("${user}MLMLOGIN");
  
    $token = "";
    if (MLM::login($user, $pass)) {
      $token = MLM::getToken();
    }
    header("Location: listindex.php?token=$token");
  } else if ($_POST['action'] == "addtestlist") {
    $topicid = $_POST['listtopicid'];
    $name = $_POST['listname'];
    $unitid = $_POST['listunitid'];
    /* validate inputs */
    if (MLM::getProjectIdFromTopicId($topicid) == "") {
      echo "Bad topic ID $topicid, please use format NEPA_[numeric]_S";
      exit(0);
    }
    if (! is_numeric($unitid)) {
      echo "Bad unit ID $unitid, please use numeric format NNNNNNNN";
      exit(0);
    }
    echo "Adding test list: \n";
    echo "topic ID: $topicid\n";
    echo "name: $name\n";
    echo "unit ID: $unitid\n";
    $mlm = new MLM();
    $mlm->addList($topicid, $name, $unitid);
  } else if ($_POST['action'] == "deleteallsubscribers") {
    $topicid = $_POST['listtopicid'];
    $mlm = new MLM();
    $list = $mlm->getListByTopic($topicid);
    if ($list == NULL) {
      echo "Can't find list $topicid for deletion";
    } else {
      echo "Deleting all subscribers from list $topicid\n";
      $mlm->deleteAllSubscribersFromList($list->id);
      echo "List emptied";
    }
  } else if ($_POST['action'] == "deletelist") {
    $topicid = $_POST['listtopicid'];
    $mlm = new MLM();
    $list = $mlm->getListByTopic($topicid);
    if ($list == NULL) {
      echo "Can't find list $topicid for deletion";
    } else {
      echo "Deleting list $topicid\n";
      $mlm->delete($list);
      echo "List deleted";
    }
  }
} else {
  echo "Invalid request method: " . $_SERVER['REQUEST_METHOD'];
}
?>
</pre>
</body>
</html>
