<?php
  function get_datamart_status() {
    $ch = curl_init("http://localhost:8080/api/1_0/units/110108/projects");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, 'admin:D0ntP4niC');
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $httpcode;
  }

  function get_datamart_statuses() {
    $call1 = 'units/110108/projects';
    $call2 = 'users/nvercruysse';
    $call3 = 'ref/units';
    $response1 = make_datamart_call($call1);
    $response2 = make_datamart_call($call2);
    $response3 = make_datamart_call($call3);
    $ret_arr = array();
    $ret_arr[] = $response1 == 200 ? 1 : 0;
    $ret_arr[] = $response2 == 200 ? 1 : 0;
    $ret_arr[] = $response3 == 200 ? 1 : 0;
    return $ret_arr;
  }

  function make_datamart_call($post_1_0) {
    $ch = curl_init("http://localhost:8080/api/1_0/$post_1_0");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, 'admin:D0ntP4niC');
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $httpcode;
  }
?>
