<?php
  require_once("/var/www/html/dmdpilot/proxies/drmProxy.php");
  $UCM_SOAP_NAME = "coreprod2";
  $FILEID = "FSTST_000001";
  //TODO: put the usernames and passwords in a conf file, same with the
  //urls
  $BROKER_URL = "http://br0k3r:s3rv1c3@127.0.0.1/dmdpilot/broker/broker.php";
  $UCM_URL = "http://sysadmin:p1cgorcl@oracleprod2/web/idcplg";
  ob_start();
  $echoStatus = testEcho($BROKER_URL);
  $echoMsgs = ob_get_contents();
  ob_end_clean();
  ob_start();
  $getFileDataStatus = testGetFileData($BROKER_URL, $FILEID);
  $getFileDataMsgs = ob_get_contents();
  ob_end_clean();
  ob_start();
  
  $scheduleStatus = testScheduler($BROKER_URL);
  $scheduleMsgs = ob_get_contents();
  ob_end_clean();
  if (isset($_GET['for_humanz'])) {
    if ($_GET['for_humanz']){
      print_html($echoStatus, $echoMsgs,
                 $getFileDataStatus, $getFileDataMsgs,
                 $scheduleStatus, $scheduleMsgs);
    } else {
      print($echoStatus.$getFileDataStatus.$scheduleStatus);
    }
  } else {
    print($echoStatus.$getFileDataStatus.$scheduleStatus);
  }
  // ob_start();
  // $ftpStatus = testFTP();
  // $ftpMsgs = ob_get_contents();
  // ob_end_clean();


  function testEcho($url) {
    $testVal = "hi there";
    $xmlrpcClient = new xmlrpc_client($url);
    $xmlrpcMsg = xmlrpc_encode_request("echoMsg", array($testVal));
    $xmlrpcResponse = $xmlrpcClient->send($xmlrpcMsg);
    if (xmlrpc_get_type($xmlrpcResponse) == "string") {
      if (strcmp($xmlrpcResponse, $testVal) == 0) {
        print "echoMsg succeeded\n";
        return 1;
      } else {
        print "echoMsg response incorrect! Returned [$xmlrpcResponse]\n";
        return 0;
      }
    } else {
      printErrorResponse($xmlrpcResponse, "echoMsg failed!\n");
      return 0;
    }
  }

  function testGetFileData($url, $fileId) {
    $xmlrpcClient = new xmlrpc_client($url);
    $xmlrpcMsg = xmlrpc_encode_request("getFileData", array($fileId));
    $xmlrpcResponse = $xmlrpcClient->send($xmlrpcMsg);
    if (xmlrpc_get_type($xmlrpcResponse) == "base64") {
      $file = $xmlrpcResponse->scalar;
      print "getFileData($fileId) succeeded\n";
      return 1;
    } else {
      printErrorResponse($xmlrpcResponse, "getFileData($fileId) failed!\n");
      return 0;
    }
  }

  function testScheduler($url) {
    try {
      $urlPath = parse_url($broker_url, PHP_URL_PATH);
      $urlArray = explode('/', $urlPath);
      $instance = $urlArray[1];
      $tmpDir = "/var/www/html/dmdpilot/tmpdir";
      $inQDir = "/var/www/html/dmdpilot/job_queues/in_q";
      $logDir = "/var/www/html/dmdpilot/logs";
      $schedulerLockFile = "${tmpDir}/lock.scheduler";
      $schedulerPidFile = "${tmpDir}/pid.scheduler";
    } catch (Exception $e) {
      print "Unable to find job control files!\n";
      return 0;
    }

    $lockinfo = @stat($schedulerLockFile);
    if (!$lockinfo) {
      print "No lockfile present!\n";
      return 0;
    }

    $pidinfo = @stat($schedulerPidFile);
    if (!$pidinfo) {
      print "No PID present!\n";
      return 0;
    }

    $pid = @file_get_contents($schedulerPidFile);
    if (!$pid) {
      print "Can't get PID!\n";
      return 0;
    }
    $pid = trim($pid);

    $procinfo = "/proc/${pid}/cmdline";
    $cmd = @file_get_contents($procinfo);
    if (!$cmd) {
      print "Can't get command line from /proc/${pid}/cmdline!\n";
      return 0;
    }

    if (!strpos($cmd, "scheduler")) {
      print "PID ${pid} does not appear to be the scheduler script!\n";
      return 0;
    }

    $dirlist = scandir($inQDir);
    $dirlist = array_filter($dirlist, "filevisible");
    print "There are " . count($dirlist) . " jobs pending\n";

    $stat = stat("${logDir}/job_detail.log");
    $ago = time() - $stat['mtime'];       // time since last update, in sec
    print "Last job was handled " . $ago . " seconds ago\n";

    //job shouldn't take longer than, say, 20min to complete
    $job_time_limit = 20 * 60;
    if ((count($dirlist) > 0) && ($ago > $job_time_limit)) {
      print "Last job was longer than 20 minutes ago!  Check for errors\n";
      return 0;
    }

    print "Job scheduler is running\n";
    return 1;
  }

  function printErrorResponse($resp, $msg) {
    print "$msg";
    print "Fault code " . $resp["faultCode"] . "\n";
    print "Fault message " . $resp["faultString"] . "\n";
  }

  class xmlrpc_client {
    private $ch;	// curl handle

    function __construct($url) {
        $this->ch=curl_init();
        curl_setopt($this->ch, CURLOPT_URL, "$url");
        // Return a variable instead of posting it directly
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        $httpheaders = array('Content-type: text/xml');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $httpheaders);

        curl_setopt($this->ch, CURLOPT_POST, True);
    }

    public function setCredentials($user, $pass) {
        $AUTH_USERPWD = utf8_encode("$user:$pass");
        curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->ch, CURLOPT_USERPWD, $AUTH_USERPWD);
    }

    // $bool should be True/False
    public function setVerbose($bool) {
        curl_setopt($this->ch, CURLOPT_VERBOSE, $bool);
    }

    public function send($msg) {
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $msg);
        $resultxml = curl_exec($this->ch);
        $info = curl_getinfo($this->ch);
        $httpcode = $info["http_code"];
        curl_close($this->ch);
        // All requests (even failures) should return with a 20x code if the
        // server didn't collapse
        if (($httpcode < 200) || ($httpcode > 299)) {
          $infotxt = print_r($info, True);
          $fakeResponse = array(
              'faultString' => "Error from XMLRPC call: $infotxt",
              'faultCode' => $httpcode
          );
          return $fakeResponse;
      } else {
          $method = "";
          $result = xmlrpc_decode_request($resultxml, $method);
          return $result;
      }
    }
  }

  function print_html($echoStat, $echoMsg, $fileDataStat, $fileDataMsg,
                      $schedStat, $schedMsg) {

    $html = '<html><body style="background: #1d1f21; font-size: 15pt">';
    $endtag = '</body></html>';
    $successCol = '#66cc66';
    $failCol = '#e36666';
    $stText = '#f8f8f8';
    $echoStatTag = '<span style="color: ';
    $fileStatTag = $echoStatTag;
    $schedStatTag = $echoStatTag;
    $echoExplTag = '<span style="color: #f8f8f8">First digit: </span>'.
                   '<span style="color: ';
    $fileExplTag = '<span style="color: #f8f8f8">Second digit: </span>'.
                   '<span style="color: ';
    $schedExplTag = '<span style="color: #f8f8f8">Third digit: </span>'.
                   '<span style="color: ';
    $echoSuccessTag = $successCol . '">XMLRPC functioning</span><br>';
    $echoFailTag = $failCol . '">XMLRPC Broken</span><br>';
    $fileSuccessTag = $successCol . '">UCM Connected</span><br>';
    $fileFailTag = $failCol . '">UCM Disconnected</span><br>';
    $schedSuccessTag = $successCol . '">Scheduler Running</span><br>';
    $schedFailTag = $failCol . '">Scheduler NOT Running</span><br>';
    $errorMsgs = '<br><p style="color: #f8f8f8">Error Messages:</p>';
    $outputErrors = $errorMsgs;
    if ($echoStat == 1) {
      $echoStatTag .= $successCol . '">1 </span>';
      $echoExplTag .= $echoSuccessTag;
    } else {
      $echoStatTag .= $failCol . '">0 </span>';
      $echoExplTag .= $echoFailTag;
      $outputErrors .= '<p style="color: #f8f8f8">';
      $outputErrors .= "{$echoMsg}</p>";
    }
    if ($fileDataStat == 1) {
      $fileStatTag .= $successCol . '">1 </span>';
      $fileExplTag .= $fileSuccessTag;
    } else {
      $fileStatTag .= $failCol . '">0 </span>';
      $fileExplTag .= $fileFailTag;
      $outputErrors .= '<p style="color: #f8f8f8">';
      $outputErrors .= "{$fileDataMsg}</p>";
    }
    if ($schedStat == 1) {
      $schedStatTag .= $successCol . '">1 </span>';
      $schedExplTag .= $schedSuccessTag;
    } else {
      $schedStatTag .= $failCol . '">0 </span>';
      $schedExplTag .= $schedFailTag;
      $outputErrors .= '<p style="color: #f8f8f8">';
      $outputErrors .= "{$schedMsg}</p>";
    }
    $html .= $echoStatTag . $fileStatTag . $schedStatTag;
    $html .= '<br>';
    $html .= $echoExplTag . $fileExplTag . $schedExplTag;
    if ($outputErrors != $errorMsgs) {
      $html .= $outputErrors;
    }
    $html .= $endtag;
    print($html);
  }

?>
