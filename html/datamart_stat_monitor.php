<?php
  require_once('/var/www/html/datamart_call.php');
  $codes = get_datamart_statuses();
  if (isset($_GET['for_humanz'])) {
    if ($_GET['for_humanz'] == 1 ||
        strcasecmp($_GET['for_humanz'], 'true') == 0) {
      print_html($codes);
    } else {
      print(implode($codes));
    }
  } else {
      print(implode($codes));
  }

  function print_html($codes) {
    $html = '<html><body style="background: #1d1f21; font-size: 15pt">';
    $endtag = '</body></html>';
    $successCol = '#66cc66';
    $failCol = '#e36666';
    $stText = '#f8f8f8';
    $projStatTag = '<span style="color: ';
    $userStatTag = $projStatTag;
    $unitStatTag = $projStatTag;
    $projExplTag = '<span style="color: #f8f8f8">First digit: </span>'.
                   '<span style="color: ';
    $userExplTag = '<span style="color: #f8f8f8">Second digit: </span>'.
                   '<span style="color: ';
    $unitExplTag = '<span style="color: #f8f8f8">Third digit: </span>'.
                   '<span style="color: ';
    $projSuccessTag = $successCol . '">units/[unit]/projects requests successful</span><br>';
    $projFailTag = $failCol . '">units/[unit]/projects requests failing</span><br>';
    $userSuccessTag = $successCol . '">users/[user] requests successful</span><br>';
    $userFailTag = $failCol . '">users/[user] requests failing</span><br>';
    $unitSuccessTag = $successCol . '">ref/units requests successful</span><br>';
    $unitFailTag = $failCol . '">ref/units requests failing</span><br>';
    $successTag = $successCol . '">1 </span>';
    $failTag = $failCol . '">0 </span>';
    if ($codes[0] == 1) {
      $projStatTag .= $successTag;
      $projExplTag .= $projSuccessTag;
    } else {
      $projStatTag .= $failTag;
      $projExplTag .= $projFailTag;
    }
    if ($codes[1] == 1) {
      $userStatTag .= $successTag;
      $userExplTag .= $userSuccessTag;
    } else {
      $userStatTag .= $failTag;
      $userExplTag .= $userFailTag;
    }
    if ($codes[2] == 1) {
      $unitStatTag .= $successTag;
      $unitExplTag .= $unitSuccessTag;
    } else {
      $unitStatTag .= $failTag;
      $unitExplTag .= $unitFailTag;
    }
    $html .= $projStatTag.$userStatTag.$unitStatTag;
    $html .= '<br>';
    $html .= $projExplTag;
    $html .= $userExplTag;
    $html .= $unitExplTag;
    $html .= $endtag;
    print($html);
  }
?>
